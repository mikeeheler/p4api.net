﻿#include "StdAfx.h"
#include "UnitTestFrameWork.h"
#include "TestP4BridgeServer.h"
#include "TextEncoder.h"

#include "..\p4bridge\P4BridgeClient.h"
#include "..\p4bridge\P4BridgeServer.h"

#include <strtable.h>
#include <strarray.h>

#include <conio.h>

CREATE_TEST_SUITE(TestP4BridgeServer)

TestP4BridgeServer::TestP4BridgeServer(void)
{
    UnitTestSuite::RegisterTest(ServerConnectionTest, "ServerConnectionTest");
    UnitTestSuite::RegisterTest(TestUnicodeClientToNonUnicodeServer, "TestUnicodeClientToNonUnicodeServer");
    UnitTestSuite::RegisterTest(TestUnicodeUserName, "TestUnicodeUserName");
    UnitTestSuite::RegisterTest(TestUntaggedCommand, "TestUntaggedCommand");
    UnitTestSuite::RegisterTest(TestTaggedCommand, "TestTaggedCommand");
    UnitTestSuite::RegisterTest(TestTextOutCommand, "TestTextOutCommand");
    UnitTestSuite::RegisterTest(TestBinaryOutCommand, "TestBinaryOutCommand");
    UnitTestSuite::RegisterTest(TestErrorOutCommand, "TestErrorOutCommand");
    UnitTestSuite::RegisterTest(TestGetSet, "TestGetSet");
    UnitTestSuite::RegisterTest(TestGetConfig, "TestGetConfig");
    UnitTestSuite::RegisterTest(TestIsIgnored, "TestIsIgnored");
    UnitTestSuite::RegisterTest(TestConnectionManger, "TestConnectionManger");
    //UnitTestSuite::RegisterTest(TestGetTicketFile, "TestGetTicketFile");
}


TestP4BridgeServer::~TestP4BridgeServer(void)
{
}

char unitTestDir[MAX_PATH];
char unitTestZip[MAX_PATH];
char * TestDir = "c:\\MyTestDir";
char * TestZip = "c:\\MyTestDir\\a.exe";
char * rcp_cmd = "p4d -r C:/MyTestDir -jr checkpoint.1";
char * udb_cmd = "p4d -r C:/MyTestDir -xu";
char * p4d_cmd = "p4d -p6666 -IdUnitTestServer -rC:/MyTestDir";
char * testCfgFile = "C:\\MyTestDir\\admin_space\\myP4Config.txt";
char * testCfgDir = "C:\\MyTestDir\\admin_space";
char * testIgnoreFile = "C:\\MyTestDir\\admin_space\\myP4Ignore.txt";
char * testIgnoredFile1 = "C:\\MyTestDir\\admin_space\\foofoofoo.foo";
char * testIgnoredFile2 = "C:\\MyTestDir\\admin_space\\moomoomoo.moo";

void * pi = NULL;
P4BridgeServer * ps = NULL;

char* oldConfig = NULL;
char* oldIgnore = NULL;

bool TestP4BridgeServer::Setup()
{
    // remove the test directory if it exists
    UnitTestSuite::rmDir( TestDir ) ;

    GetCurrentDirectory(sizeof(unitTestDir), unitTestDir);

    strcpy( unitTestZip, unitTestDir);
    strcat( unitTestZip, "\\a.exe");

    if (!CreateDirectory( TestDir, NULL)) return false;

    if (!CopyFile(unitTestZip, TestZip, false)) return false;

    if (!SetCurrentDirectory(TestDir)) return false;

    pi = UnitTestSuite::RunProgram("a", TestDir, true, true);
    if (!pi) 
    {
        SetCurrentDirectory(unitTestDir);
        return false;
    }

    delete pi;

    pi = UnitTestSuite::RunProgram(rcp_cmd, TestDir, true, true);
    if (!pi) 
    {
        SetCurrentDirectory(unitTestDir);
        return false;
    }

    delete pi;

    pi = UnitTestSuite::RunProgram(udb_cmd, TestDir, true, true);
    if (!pi) 
    {
        SetCurrentDirectory(unitTestDir);
        return false;
    }

    delete pi;

    pi = UnitTestSuite::RunProgram(p4d_cmd, TestDir, false, false);
    if (!pi) 
    {
        SetCurrentDirectory(unitTestDir);
        return false;
    }

//    _getch();

    return true;
}

bool TestP4BridgeServer::TearDown(char* testName)
{
	UnitTestSuite::rmDir( testCfgFile ) ;
    UnitTestSuite::rmDir( testIgnoreFile ) ;
	if (oldConfig)
	{
		P4BridgeServer::Set("P4CONFIG", oldConfig);	
		oldConfig = NULL;
	}
	if (oldIgnore)
	{
		P4BridgeServer::Set("P4IGNORE", oldIgnore);	
		oldIgnore = NULL;
	}
    if (pi)
        UnitTestSuite::EndProcess( (LPPROCESS_INFORMATION) pi );
	if (ps)
    {
		delete ps;
		ps = NULL;

		//p4base::DumpMemoryState("After deleting server");
	}
    SetCurrentDirectory(unitTestDir);

    UnitTestSuite::rmDir( TestDir ) ;

	p4base::PrintMemoryState(testName);

    return true;
}

bool TestP4BridgeServer::ServerConnectionTest()
{
    P4ClientError* connectionError = NULL;
    // create a new server
    ps = new P4BridgeServer("localhost:6666", "admin", "", "");
    ASSERT_NOT_NULL(ps);

    // connect and see if the api returned an error. 
    if( !ps->connected( &connectionError ) )
    {
        char buff[256];
        sprintf(buff, "Connection error: %s", *connectionError);
        // Abort if the connect did not succeed
        ASSERT_FAIL(buff);
    }

    return true;
}

bool TestP4BridgeServer::TestUnicodeClientToNonUnicodeServer()
{
    P4ClientError* connectionError = NULL;
    // create a new server
    ps = new P4BridgeServer("localhost:6666", "admin", "", "admin_space");
    ASSERT_NOT_NULL(ps);

    // connect and see if the api returned an error. 
    if( !ps->connected( &connectionError ) )
    {
        char buff[256];
        sprintf(buff, "Connection error: %s", *connectionError);
        // Abort if the connect did not succeed
        ASSERT_FAIL(buff);
    }

    ASSERT_NOT_EQUAL(ps->unicodeServer(), 1);
    ps->set_charset("utf8", "utf16le");

    char* params[1];
    params[0] = "//depot/mycode/*";

    ASSERT_FALSE(ps->run_command("files", 3456, 0, params, 1))

    P4ClientError * out = ps->get_ui(3456)->GetErrorResults();

    ASSERT_STRING_STARTS_WITH(out->Message, "Unicode clients require a unicode enabled server.")

	ps->ReleaseConnection(3456,0);

    return true;
}

bool TestP4BridgeServer::TestUnicodeUserName()
{

    P4ClientError* connectionError = NULL;
    // create a new server
    //Aleksey (Alexei) in Cyrillic = "\xD0\x90\xD0\xbb\xD0\xB5\xD0\xBA\xD1\x81\xD0\xB5\xD0\xB9\0" IN utf-8
    ps = new P4BridgeServer("localhost:6666", "\xD0\x90\xD0\xBB\xD0\xB5\xD0\xBA\xD1\x81\xD0\xB5\xD0\xB9\0", "pass", "\xD0\x90\xD0\xbb\xD0\xB5\xD0\xBA\xD1\x81\xD0\xB5\xD0\xB9\0");
    ASSERT_NOT_NULL(ps);

    // connect and see if the api returned an error. 
    if( !ps->connected( &connectionError ) )
    {
        char buff[256];
        printf(buff, "Connection error: %s", *connectionError);
        // Abort if the connect did not succeed
        ASSERT_FAIL(buff);
    }

    ASSERT_FALSE(ps->unicodeServer());
    ps->set_charset("utf8", "utf16le");

    char* params[1];
    params[0] = "//depot/mycode/*";

    ASSERT_FALSE(ps->run_command("files", 7, 0, params, 1))

    P4ClientError * out = ps->get_ui(7)->GetErrorResults();

    ASSERT_STRING_STARTS_WITH(out->Message, "Unicode clients require a unicode enabled server.")

    return true;
}

bool TestP4BridgeServer::TestUntaggedCommand()
{
    P4ClientError* connectionError = NULL;
    // create a new server
    ps = new P4BridgeServer("localhost:6666", "admin", "", "admin_space");
    ASSERT_NOT_NULL(ps);

    // connect and see if the api returned an error. 
    if( !ps->connected( &connectionError ) )
    {
        char buff[256];
        sprintf(buff, "Connection error: %s", *connectionError);
        // Abort if the connect did not succeed
        ASSERT_FAIL(buff);
    }
    char* params[1];
    params[0] = "//depot/mycode/*";

    ASSERT_TRUE(ps->run_command("files", 7, 0, params, 1))

    P4ClientInfoMsg * out = ps->get_ui(7)->GetInfoResults();

    ASSERT_STRING_STARTS_WITH(out->Message, "//depot/MyCode/")

    return true;
}

bool TestP4BridgeServer::TestTaggedCommand()
{
    P4ClientError* connectionError = NULL;
    // create a new server
    ps = new P4BridgeServer("localhost:6666", "admin", "", "admin_space");
    ASSERT_NOT_NULL(ps);

    // connect and see if the api returned an error. 
    if( !ps->connected( &connectionError ) )
    {
        char buff[256];
        sprintf(buff, "Connection error: %s", *connectionError);
        // Abort if the connect did not succeed
        ASSERT_FAIL(buff);
    }
    char* params[1];
    params[0] = "//depot/mycode/*";

    ASSERT_TRUE(ps->run_command("files", 7, 1, params, 1))

    StrDictListIterator * out = ps->get_ui(7)->GetTaggedOutput();

    ASSERT_NOT_NULL(out);

    int itemCnt = 0;
    while (StrDictList * pItem = out->GetNextItem())
    {
        int entryCnt = 0;

        while (KeyValuePair * pEntry = out->GetNextEntry())
        {
            if ((itemCnt == 0) && (strcmp(pEntry->key, "depotFile") == 0))
                ASSERT_STRING_STARTS_WITH(pEntry->value, "//depot/MyCode/")
            if ((itemCnt == 1) && (strcmp(pEntry->key, "depotFile") == 0))
                ASSERT_STRING_STARTS_WITH(pEntry->value, "//depot/MyCode/")
            entryCnt++;
        }
        ASSERT_NOT_EQUAL(entryCnt, 0);
        itemCnt++;
    }
    ASSERT_EQUAL(itemCnt, 3);

	delete out;

    return true;
}

bool TestP4BridgeServer::TestTextOutCommand()
{
    P4ClientError* connectionError = NULL;
    // create a new server
    ps = new P4BridgeServer("localhost:6666", "admin", "", "admin_space");
    ASSERT_NOT_NULL(ps);

    // connect and see if the api returned an error. 
    if( !ps->connected( &connectionError ) )
    {
        char buff[256];
        sprintf(buff, "Connection error: %s", *connectionError);
        // Abort if the connect did not succeed
        ASSERT_FAIL(buff);
    }
    char* params[1];
    params[0] = "//depot/MyCode/ReadMe.txt";

    ASSERT_TRUE(ps->run_command("print", 7, 1, params, 1))

    StrBuf * out = ps->get_ui(7)->GetTextResults();

    ASSERT_NOT_NULL(out);

    ASSERT_STRING_EQUAL(out->Text(), "Don't Read This!\n\nIt's Secret!")

    return true;
}

bool TestP4BridgeServer::TestBinaryOutCommand()
{
    P4ClientError* connectionError = NULL;
    // create a new server
    ps = new P4BridgeServer("localhost:6666", "admin", "", "admin_space");
    ASSERT_NOT_NULL(ps);

    // connect and see if the api returned an error. 
    if( !ps->connected( &connectionError ) )
    {
        char buff[256];
        sprintf(buff, "Connection error: %s", *connectionError);
        // Abort if the connect did not succeed
        ASSERT_FAIL(buff);
    }
    char* params[1];
    params[0] = "//depot/MyCode/Silly.bmp";

    ASSERT_TRUE(ps->run_command("print", 7, 1, params, 1))

    int cnt = ps->get_ui(7)->GetBinaryResultsCount();

    ASSERT_EQUAL(cnt, 3126)

    void * out = ps->get_ui(7)->GetBinaryResults();

    ASSERT_NOT_NULL(out);
    ASSERT_EQUAL((*(((unsigned char*)out) + 1)), 0x4d)

    return true;
}

bool TestP4BridgeServer::TestErrorOutCommand()
{
    P4ClientError* connectionError = NULL;
    // create a new server
    ps = new P4BridgeServer("localhost:6666", "admin", "", "admin_space");
    ASSERT_NOT_NULL(ps);

    // connect and see if the api returned an error. 
    if( !ps->connected( &connectionError ) )
    {
        char buff[256];
        sprintf(buff, "Connection error: %s", *connectionError);
        // Abort if the connect did not succeed
        ASSERT_FAIL(buff);
    }
    char* params[1];
    params[0] = "//depot/MyCode/Billy.bmp";

    // run a command against a nonexistent file
    // Should fail
    ASSERT_FALSE(ps->run_command("rent", 7, 1, params, 1))

    P4ClientError * out = ps->get_ui(7)->GetErrorResults();

    ASSERT_NOT_NULL(out);

    //ASSERT_STRING_STARTS_WITH(out->Message, "Unknown command.  Try 'p4 help' for info")
	ASSERT_EQUAL(out->ErrorCode, 805379098);
    ASSERT_NULL(out->Next)

    return true;
}

bool TestP4BridgeServer::TestGetSet()
{
	char *expected = "C:\\login.bat";

	P4BridgeServer::Set("P4FOOBAR", expected);

	char *result = P4BridgeServer::Get("P4FOOBAR");

	ASSERT_STRING_EQUAL(expected, result);
	delete[] result;

	P4BridgeServer::Set("P4FOOBAR", NULL);
	result = P4BridgeServer::Get("P4FOOBAR");
	ASSERT_NULL(result);

	wchar_t *w_expected = L"C:\\login<АБВГ>.bat";
	expected = TextEncoder::Utf16ToUtf8(w_expected);

	P4BridgeServer::Set("P4FOOBAR", expected);

	result = P4BridgeServer::Get("P4FOOBAR");

	ASSERT_STRING_EQUAL(expected, result);

	wchar_t *w_result = TextEncoder::Utf8ToUtf16(result);

	ASSERT_W_STRING_EQUAL(w_expected, w_result);

	delete[] result;

	P4BridgeServer::Set("P4FOOBAR", NULL);
	result = P4BridgeServer::Get("P4FOOBAR");
	ASSERT_NULL(result);

   return true;
}

bool TestP4BridgeServer::TestGetConfig()
{
	OFSTRUCT ReOpenBuff;

	oldConfig = P4BridgeServer::Get("P4CONFIG");

	HFILE hf = OpenFile(testCfgFile, &ReOpenBuff, OF_CREATE);
	DWORD written = 0;
	WriteFile((HANDLE) hf, (LPVOID) "P4CLIENT testClient\r\n", 21, &written, NULL);
	CloseHandle((HANDLE) hf);

	P4BridgeServer::Set("P4CONFIG", "myP4Config.txt");	

	char *result = P4BridgeServer::get_config(testCfgDir);

	ASSERT_STRING_EQUAL(testCfgFile, result);

	delete[] result;

    P4ClientError* connectionError = NULL;
    // create a new server
    ps = new P4BridgeServer("localhost:6666", "admin", "", "admin_space");
    ASSERT_NOT_NULL(ps);

	ps->set_cwd(testCfgDir);

    //// connect and see if the api returned an error. 
    //if( !ps->connected( connectionError ) )
    //{
    //    char buff[256];
    //    sprintf(buff, "Connection error: %s", *connectionError);
    //    // Abort if the connect did not succeed
    //    ASSERT_FAIL(buff);
    //}

	result = P4BridgeServer::Get("P4CONFIG");
	
	ASSERT_STRING_EQUAL("myP4Config.txt", result);

	result = ps->get_config();

	ASSERT_STRING_EQUAL(testCfgFile, result);

    return true;
}

bool TestP4BridgeServer::TestIsIgnored()
{
	OFSTRUCT ReOpenBuff;

	oldIgnore = P4BridgeServer::Get("P4IGNORE");

	HFILE hf = OpenFile(testIgnoreFile, &ReOpenBuff, OF_CREATE);
	DWORD written = 0;
	WriteFile((HANDLE) hf, (LPVOID) "foofoofoo.foo\r\n", 15, &written, NULL);
	CloseHandle((HANDLE) hf);

	P4BridgeServer::Set("P4IGNORE", "myP4Ignore.txt");	

	ASSERT_TRUE(P4BridgeServer::IsIgnored(StrRef(testIgnoredFile1)));
	ASSERT_FALSE(P4BridgeServer::IsIgnored(StrRef(testIgnoredFile2)));

	return true;
}

bool TestP4BridgeServer::TestConnectionManger()
{
    P4ClientError* connectionError = NULL;
    // create a new server
    ps = new P4BridgeServer("localhost:6666", "admin", "", "admin_space");
    ASSERT_NOT_NULL(ps);

    // connect and see if the api returned an error. 
    if( !ps->connected( &connectionError ) )
    {
        char buff[256];
        sprintf(buff, "Connection error: %s", *connectionError);
        // Abort if the connect did not succeed
        ASSERT_FAIL(buff);
    }
    char* params[1];
    params[0] = "//depot/mycode/*";

	//p4base::DumpMemoryState("Before first command run");

	ASSERT_TRUE(ps->run_command("files", 6, 1, params, 1))

	//p4base::DumpMemoryState("After first command run");

	ps->ReleaseConnection(6, 10);

	//p4base::DumpMemoryState("After first ReleaseConnection");

    ASSERT_TRUE(ps->run_command("files", 7, 1, params, 1))

	//p4base::DumpMemoryState("After second command run");

	ps->ReleaseConnection(7, 10);

	//p4base::DumpMemoryState("After second ReleaseConnection");

	int idleCnt = ps->FreeConnections(0);

    ASSERT_EQUAL(idleCnt, 1)

	//p4base::DumpMemoryState("After first FreeConnections");

    ASSERT_TRUE(ps->run_command("files", 8, 1, params, 1))	

	//p4base::DumpMemoryState("After third command run");

    ASSERT_TRUE(ps->run_command("files", 9, 1, params, 1))

	//p4base::DumpMemoryState("After fourth command run");

	ps->ReleaseConnection(8, 20);
	//p4base::DumpMemoryState("After third ReleaseConnection");
	ps->ReleaseConnection(9, 30);
	//p4base::DumpMemoryState("After fourth ReleaseConnection");

	idleCnt = ps->FreeConnections(11);
    ASSERT_EQUAL(idleCnt, 2)
	//p4base::DumpMemoryState("After second FreeConnections");

	idleCnt = ps->FreeConnections(21);
    ASSERT_EQUAL(idleCnt, 1)
	//p4base::DumpMemoryState("After third FreeConnections");

	idleCnt = ps->FreeConnections(31);
    ASSERT_EQUAL(idleCnt, 0)
	//p4base::DumpMemoryState("After fourth FreeConnections");

    return true;
}

//bool TestP4BridgeServer::TestGetTicketFile()
//{
//	char* tktfile = P4BridgeServer::GetTicketFile();
//
//	ASSERT_NOT_NULL(tktfile);
//
//	delete tktfile;
//
//	return true;
//}
