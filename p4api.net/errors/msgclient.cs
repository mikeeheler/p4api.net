using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Text;

namespace Perforce.P4
{
	public partial class P4ClientError
	{
		public static int MsgClient_Connect = ErrorOf(ErrorSubsystem.ES_CLIENT, 1, ErrorSeverity.E_FAILED, ErrorGeneric.EV_COMM, 0); //"Connect to server failed; check $P4PORT."
		public static int MsgClient_Fatal = ErrorOf(ErrorSubsystem.ES_CLIENT, 2, ErrorSeverity.E_FATAL, ErrorGeneric.EV_CLIENT, 0); //"Fatal client error; disconnecting!"
		public static int MsgClient_BadFlag = ErrorOf(ErrorSubsystem.ES_CLIENT, 3, ErrorSeverity.E_FAILED, ErrorGeneric.EV_USAGE, 0); //"Unknown flag.  Try ? for help."
		public static int MsgClient_ClobberFile = ErrorOf(ErrorSubsystem.ES_CLIENT, 4, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 1); //"Can't clobber writable file %file%"
		public static int MsgClient_MkDir = ErrorOf(ErrorSubsystem.ES_CLIENT, 5, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 1); //"can't create directory for %file%"
		public static int MsgClient_Eof = ErrorOf(ErrorSubsystem.ES_CLIENT, 6, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 0); //"EOF reading terminal."
		public static int MsgClient_FileOpenError = ErrorOf(ErrorSubsystem.ES_CLIENT, 40, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 0); //"Error opening file."
		public static int MsgClient_CantEdit = ErrorOf(ErrorSubsystem.ES_CLIENT, 7, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 1); //"%type% - can't edit this type of file!"
		public static int MsgClient_NoMerger = ErrorOf(ErrorSubsystem.ES_CLIENT, 8, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 0); //"No merge program specified with $P4MERGE or $MERGE."
		public static int MsgClient_ToolServer2 = ErrorOf(ErrorSubsystem.ES_CLIENT, 10, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 0); //"Could not start another ToolServer.  Tool is running under ToolServer."
		public static int MsgClient_ToolServer = ErrorOf(ErrorSubsystem.ES_CLIENT, 11, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 0); //"Could not start ToolServer."
		public static int MsgClient_ToolCmdCreate = ErrorOf(ErrorSubsystem.ES_CLIENT, 12, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 0); //"Could not create command to send to ToolServer."
		public static int MsgClient_ToolCmdSend = ErrorOf(ErrorSubsystem.ES_CLIENT, 13, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 0); //"Could not send command to ToolServer."
		public static int MsgClient_Memory = ErrorOf(ErrorSubsystem.ES_CLIENT, 14, ErrorSeverity.E_FATAL, ErrorGeneric.EV_CLIENT, 0); //"Out of memory!"
		public static int MsgClient_CantFindApp = ErrorOf(ErrorSubsystem.ES_CLIENT, 15, ErrorSeverity.E_FATAL, ErrorGeneric.EV_CLIENT, 0); //"Could not find application!"
		public static int MsgClient_BadSignature = ErrorOf(ErrorSubsystem.ES_CLIENT, 16, ErrorSeverity.E_FATAL, ErrorGeneric.EV_CLIENT, 0); //"EDITOR_SIGNATURE must be 4 characters exactly!"
		public static int MsgClient_BadMarshalInput = ErrorOf(ErrorSubsystem.ES_CLIENT, 29, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 0); //"Invalid marshalled data supplied as input."
		public static int MsgClient_ResolveManually = ErrorOf(ErrorSubsystem.ES_CLIENT, 18, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 0); //"Must resolve manually."
		public static int MsgClient_NonTextFileMerge = ErrorOf(ErrorSubsystem.ES_CLIENT, 19, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 0); //"Non-text file merge."
		public static int MsgClient_MergeMsg2 = ErrorOf(ErrorSubsystem.ES_CLIENT, 28, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 4); //"Non-text diff: %yours% yours + %theirs% theirs + %both% both + %conflicting% conflicting"
		public static int MsgClient_MergeMsg3 = ErrorOf(ErrorSubsystem.ES_CLIENT, 20, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 4); //"Diff chunks: %yours% yours + %theirs% theirs + %both% both + %conflicting% conflicting"
		public static int MsgClient_MergeMsg32 = ErrorOf(ErrorSubsystem.ES_CLIENT, 21, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 1); //"Diff chunks: %chunks% between yours and theirs (no common base)"
		public static int MsgClient_MergePrompt = ErrorOf(ErrorSubsystem.ES_CLIENT, 22, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 1); //"Accept(a) Edit(e) Diff(d) Merge (m) Skip(s) Help(?) [%mergeDefault%]: "
		public static int MsgClient_MergePrompt2 = ErrorOf(ErrorSubsystem.ES_CLIENT, 26, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 1); //"Accept (at/ay) Skip (s) Help (?) [%mergeDefault%]: "
		public static int MsgClient_MergePrompt2Edit = ErrorOf(ErrorSubsystem.ES_CLIENT, 27, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 1); //"Accept (at/ay) Edit (et/ey) Skip (s) Help (?) [%mergeDefault%]: "
		public static int MsgClient_ConfirmMarkers = ErrorOf(ErrorSubsystem.ES_CLIENT, 23, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 0); //"There are still change markers: confirm accept (y/n)? "
		public static int MsgClient_ConfirmEdit = ErrorOf(ErrorSubsystem.ES_CLIENT, 24, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 0); //"Use 'ae' to indicate original edits: confirm accept merge (y/n)? "
		public static int MsgClient_Confirm = ErrorOf(ErrorSubsystem.ES_CLIENT, 25, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 0); //"This overrides your changes: confirm accept (y/n)? "
		public static int MsgClient_CheckFileAssume = ErrorOf(ErrorSubsystem.ES_CLIENT, 30, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 3); //"%file% - %type%, assuming %type2%."
		public static int MsgClient_CheckFileAssumeWild = ErrorOf(ErrorSubsystem.ES_CLIENT, 41, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 4); //"%file% - %type%, assuming %type2% (%modified%)."
		public static int MsgClient_CheckFileSubst = ErrorOf(ErrorSubsystem.ES_CLIENT, 31, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 3); //"%file% - using %type% instead of %type2%"
		public static int MsgClient_CheckFileCant = ErrorOf(ErrorSubsystem.ES_CLIENT, 32, ErrorSeverity.E_INFO, ErrorGeneric.EV_CLIENT, 2); //"%file% - %type% file can't be added."
		public static int MsgClient_FileExists = ErrorOf(ErrorSubsystem.ES_CLIENT, 34, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 1); //"%file% - can't overwrite existing file."
		public static int MsgClient_NoSuchFile = ErrorOf(ErrorSubsystem.ES_CLIENT, 35, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 1); //"%file% - file does not exist."
		public static int MsgClient_LoginPrintTicket = ErrorOf(ErrorSubsystem.ES_CLIENT, 36, ErrorSeverity.E_INFO, ErrorGeneric.EV_NONE, 1); //"%ticket%"
		public static int MsgClient_DigestMisMatch = ErrorOf(ErrorSubsystem.ES_CLIENT, 37, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 3); //"%clientFile% corrupted during transfer (or bad on the server) %clientDigest% vs %serverDigest%"
		public static int MsgClient_NotUnderPath = ErrorOf(ErrorSubsystem.ES_CLIENT, 38, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 2); //"File %clientFile% is not inside permitted filesystem path %clientPath%"
		public static int MsgClient_LineTooLong = ErrorOf(ErrorSubsystem.ES_CLIENT, 39, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 1); //"Command line too long. Maximum length is %maxLineLen%. This length can be increased by setting filesys.bufsize in P4CONFIG or using -v."
		public static int MsgClient_ZCResolve = ErrorOf(ErrorSubsystem.ES_CLIENT, 33, ErrorSeverity.E_FAILED, ErrorGeneric.EV_COMM, 2); //"Zeroconf resolved '%name%' to '%port%'."
	}
}
