using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Text;

namespace Perforce.P4
{
	public partial class P4ClientError
	{
		public static int MsgOs_Sys = ErrorOf(ErrorSubsystem.ES_OS, 1, ErrorSeverity.E_FAILED, ErrorGeneric.EV_FAULT, 3); //"%operation%: %arg%: %errmsg%"
		public static int MsgOs_Sys2 = ErrorOf(ErrorSubsystem.ES_OS, 9, ErrorSeverity.E_FAILED, ErrorGeneric.EV_FAULT, 3); //"%operation2%: %arg2%: %errmsg2%"
		public static int MsgOs_SysUn = ErrorOf(ErrorSubsystem.ES_OS, 2, ErrorSeverity.E_FAILED, ErrorGeneric.EV_FAULT, 3); //"%operation%: %arg%: unknown errno %errno%"
		public static int MsgOs_SysUn2 = ErrorOf(ErrorSubsystem.ES_OS, 10, ErrorSeverity.E_FAILED, ErrorGeneric.EV_FAULT, 3); //"%operation2%: %arg2%: unknown errno %errno2%"
		public static int MsgOs_ChmodBetrayal = ErrorOf(ErrorSubsystem.ES_OS, 11, ErrorSeverity.E_FATAL, ErrorGeneric.EV_FAULT, 4); //"File mode modification failed! File %oldname% was successfully renamed to %newname% but the file permissions were not correctly changed to read-only. The current permissions are %perms% and the file inode number is %inode%."
		public static int MsgOs_Net = ErrorOf(ErrorSubsystem.ES_OS, 3, ErrorSeverity.E_FAILED, ErrorGeneric.EV_COMM, 3); //"%operation%: %arg%: %errmsg%"
		public static int MsgOs_NetUn = ErrorOf(ErrorSubsystem.ES_OS, 4, ErrorSeverity.E_FAILED, ErrorGeneric.EV_COMM, 3); //"%operation%: %arg%: unknown network error %errno%"
		public static int MsgOs_TooMany = ErrorOf(ErrorSubsystem.ES_OS, 5, ErrorSeverity.E_FATAL, ErrorGeneric.EV_FAULT, 1); //"%handle%: too many handles!"
		public static int MsgOs_Deleted = ErrorOf(ErrorSubsystem.ES_OS, 6, ErrorSeverity.E_FATAL, ErrorGeneric.EV_FAULT, 1); //"%handle%: deleted handled!"
		public static int MsgOs_NoSuch = ErrorOf(ErrorSubsystem.ES_OS, 7, ErrorSeverity.E_FATAL, ErrorGeneric.EV_FAULT, 1); //"%handle%: no such handle!"
		public static int MsgOs_EmptyFork = ErrorOf(ErrorSubsystem.ES_OS, 8, ErrorSeverity.E_FAILED, ErrorGeneric.EV_CLIENT, 1); //"Resource fork for %file% from server is empty."
		public static int MsgOs_NameTooLong = ErrorOf(ErrorSubsystem.ES_OS, 12, ErrorSeverity.E_FAILED, ErrorGeneric.EV_FAULT, 3); //"Filename '%filename%' is length %actual% which exceeds the internal length limit of %maxlen%."
	}
}
