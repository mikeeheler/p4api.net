#pragma once

class ConnectionManager;
class P4BridgeClient;
class P4BridgeServer;

#ifdef _DEBUG_MEMORY
class P4Connection : public ClientApi, public KeepAlive, public DoublyLinkedListItem, p4base
#else
class P4Connection : public ClientApi, public KeepAlive, public DoublyLinkedListItem
#endif
{
private:
	P4Connection(void);

	// KeepAlive status
	int	isAlive;
	
public:
	P4Connection(ConnectionManager* conMgr, P4BridgeServer* pServer, int cmdId);
	virtual ~P4Connection(void);
	
	// has the client been initialized
	int clientNeedsInit;

	void Disconnect( void );

	// KeepAlive functionality
	virtual int	IsAlive();
	void IsAlive(int val) {isAlive = val;}

	void cancel_command();

	P4BridgeClient* ui;

	void SetCharset( CharSetApi::CharSet c, CharSetApi::CharSet filec );

	unsigned _int64 ReleaseTime;

#ifdef _DEBUG_MEMORY
	    // Simple type identification for registering objects
	virtual int Type(void) {return tP4Connection;}
#endif
};

