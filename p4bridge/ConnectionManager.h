#pragma once

class P4Connection;
class StrPtr;
class P4BridgeServer;

#ifndef time
#include "time.h";
#endif

//forward references
class ConnectionManager;

//#ifdef _DEBUG_MEMORY
//class IdleConnection : public DoublyLinkedListItem, p4base
//#else
//class IdleConnection : public DoublyLinkedListItem
//#endif
//{
//public:
//	unsigned _int64 ReleaseTime;
//	P4Connection* pCon;
//
//#ifdef _DEBUG_MEMORY
//	IdleConnection(P4Connection* pcon, _int64 releaseTime);
//#else
//	IdleConnection(P4Connection* pcon, _int64 releaseTime)
//		: DoublyLinkedListItem(NULL) { pCon = pcon; ReleaseTime = releaseTime;} 
//#endif
//
//	virtual ~IdleConnection(void);
//
//#ifdef _DEBUG_MEMORY
//	    // Simple type identification for registering objects
//	virtual int Type(void) {return tIdleConnection;}
//#endif
//};

#ifdef _DEBUG_MEMORY
class ConnectionManager : public DoublyLinkedList, p4base
#else
class ConnectionManager : public DoublyLinkedList
#endif
{
private:
	char* client;
	char* user;
	char* port;
	char* password;
	char* programName;
	char* programVer;
	char* cwd;

	CharSetApi::CharSet charset;
	CharSetApi::CharSet file_charset;

	P4BridgeServer *pServer;

	ConnectionManager(void);

	DoublyLinkedList* IdleConnections;

	void DiscardConnections();

public:
	int Initialized;

	ConnectionManager(P4BridgeServer *pserver);

	virtual ~ConnectionManager(void);

	P4Connection* GetConnection(int cmdId);
	void ReleaseConnection(int cmdId, unsigned _int64 releaseTime);
	void ReleaseConnection(P4Connection* pCon, unsigned _int64 releaseTime);
	int FreeConnections( unsigned _int64 currentTime );
	P4Connection* NewConnection(int cmdId);
	//P4Connection* DefaultConnection();

	// Set the connection data used
	void SetClient( const char* newVal );
	void SetUser( const char* newVal );
	void SetPort( const char* newVal );
	void SetPassword( const char* newVal );
	void SetProgramName( const char* newVal );
	void SetProgramVer( const char* newVal );
	void SetCwd( const char* newCwd );
	void SetCharset( CharSetApi::CharSet c, CharSetApi::CharSet filec );

	void SetConnection(const char* newPort, const char* newUser, const char* newPassword, const char* newClient)
		{SetClient(newClient); SetUser(newUser);SetPort(newPort);SetPassword(newPassword);}

	const StrPtr* GetCharset( );

	int Disconnect( void );

#ifdef _DEBUG_MEMORY
	    // Simple type identification for registering objects
	virtual int Type(void) {return tConnectionManager;}
#endif
};

