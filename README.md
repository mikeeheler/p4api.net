A .Net CLR API
--------------

### About This Project

P4 .Net provides an API for the Microsoft .Net Common Language Runtime.
It can be used from any managed language including C\#, VB.Net, and J\#.

Please see
[README.txt](https://swarm.workshop.perforce.com/files/guest/perforce_software/p4api.net/README.txt)
for more instructions on how to build and use the API.
